/*
 * xdraw.c - Base for 1. project
 */

/*
 * Standard XToolkit and OSF/Motif include files.
 */
#include <X11/Intrinsic.h>
#include <Xm/Xm.h> 
#include <Xm/MessageB.h>
#include <Xm/Protocols.h>

/*
 * Public include files for widgets used in this file.
 */
#include <Xm/MainW.h> 
#include <Xm/Form.h> 
#include <Xm/Frame.h>
#include <Xm/DrawingA.h>
#include <Xm/PushB.h>
#include <Xm/RowColumn.h>
#include <Xm/ComboBox.h>
#include <Xm/SSpinB.h>
#include <Xm/Command.h>
#include <Xm/Label.h>
#include <Xm/LabelG.h>
#include <Xm/MessageB.h>
#include <Xm/Scale.h>

/*
 * Common C library include files
 */
#include <cstdio>
#include <cstdlib>
#include <X11/Xlib.h>

/*
 * Common C++ includes
 */
#include <iostream>
#include <stdexcept>
#include <vector>
#include <memory>

#define C_STR(INPUT) ((char*)std::string(INPUT).c_str())

/**
 * Simple 2D point type.
 */
struct Point
{
    Point(int xx, int yy): 
        x(xx), y(yy)
    { }
    Point(): 
        x(0), y(0)
    { }
    int x{0};
    int y{0};
};

class Shape
{
public:
    Shape(Pixel colourF, Pixel colourB, unsigned int lineWidth,
          bool isDashed, bool isFilled) :
        mColourF(colourF), mColourB(colourB), mLineWidth(lineWidth),
        mDashed(isDashed), mFilled(isFilled)
    { }

    Shape(const Shape &o) = delete;
    Shape(const Shape &&o) = delete;

    Shape& operator=(const Shape &r) = delete;
    Shape& operator=(const Shape &&r) = delete;

    /**
     * Draw this shape on given widget, using the graphics context.
     * @param w Where to draw this shape.
     * @param gc Graphics context used for drawing.
     */
    virtual void draw(Widget w, GC gc) = 0;

    /**
     * Draw silhouette of this shape on given widget, using the graphics context.
     * @param w Where to draw this shape.
     * @param gc Graphics context used for drawing.
     */
    virtual void drawS(Widget w, GC gc) = 0;

    /**
     * Undraw silhouette of this shape on given widget, using the graphics context.
     * @param w Where to undraw this shape.
     * @param gc Graphics context used for drawing.
     */
    virtual void undrawS(Widget w, GC gc) = 0;
private:
protected:
    /// Line width.
    unsigned int mLineWidth;
    /// Is the line dashed?
    bool mDashed;
    /// Is the rectangle filled?
    bool mFilled;
    /// Foreground colour of the shape.
    Pixel mColourF;
    /// Background colour of the shape.
    Pixel mColourB;
};

class PointInfo: public Shape
{
public:
    PointInfo(Point begMousePos, Pixel colourF = 0, Pixel colourB = 0,
              unsigned int lineWidth = 0, bool isDashed = false, bool isFilled = false);

    virtual void draw(Widget w, GC gc) override final;
    virtual void drawS(Widget w, GC gc) override final;
    virtual void undrawS(Widget w, GC gc) override final;
private:
    /// Position of the point.
    Point mPos;
protected:
};

class LineInfo: public Shape
{
public:
    LineInfo(Point begMousePos, Point endMousePos, Pixel colourF = 0, Pixel colourB = 0,
             unsigned int lineWidth = 0, bool isDashed = false, bool isFilled = false);

    virtual void draw(Widget w, GC gc) override final;
    virtual void drawS(Widget w, GC gc) override final;
    virtual void undrawS(Widget w, GC gc) override final;
private:
    /// Beginning of the line.
    Point mBegin;
    /// Ending of the line.
    Point mEnd;
protected:
};

class RectangleInfo: public Shape
{
public:
    RectangleInfo(Point begMousePos, Point endMousePos, Pixel colourF = 0, Pixel colourB = 0,
                  unsigned int lineWidth = 0, bool isDashed = false, bool isFilled = false);

    virtual void draw(Widget w, GC gc) override final;
    virtual void drawS(Widget w, GC gc) override final;
    virtual void undrawS(Widget w, GC gc) override final;
private:
    /// Beginning of the rectangle.
    Point mBegin;
    /// Width of the rectangle.
    unsigned int mWidth;
    /// Height of the rectangle.
    unsigned int mHeight;
protected:
};

class EllipseInfo: public Shape
{
public:
    EllipseInfo(Point begMousePos, Point endMousePos, Pixel colourF = 0, Pixel colourB = 0,
                unsigned int lineWidth = 0, bool isDashed = false, bool isFilled = false);

    virtual void draw(Widget w, GC gc) override final;
    virtual void drawS(Widget w, GC gc) override final;
    virtual void undrawS(Widget w, GC gc) override final;
private:
    /// Where to start drawing the ellipse in degreed * 64.
    static const int DEG_BEGIN;
    /// Where to stop drawing the ellipse in degreed * 64.
    static const int DEG_END;

    /// Beginning of the rectangle.
    Point mBegin;
    /// Width of the rectangle.
    unsigned int mWidth;
    /// Height of the rectangle.
    unsigned int mHeight;
protected:
};
const int EllipseInfo::DEG_BEGIN = 0;
const int EllipseInfo::DEG_END = 360 * 64;

PointInfo::PointInfo(Point begMousePos, Pixel colourF, Pixel colourB,
                     unsigned int lineWidth, bool isDashed, bool isFilled) :
    Shape{colourF, colourB, lineWidth, isDashed, isFilled},
    mPos{begMousePos}
{ }

void PointInfo::draw(Widget w, GC gc)
{
    XSetForeground(XtDisplay(w), gc, mColourF);
    XSetBackground(XtDisplay(w), gc, mColourB);
    XSetLineAttributes(XtDisplay(w), gc, mLineWidth, LineSolid, CapRound, JoinRound);
    XDrawPoint(XtDisplay(w), XtWindow(w), gc, mPos.x, mPos.y);
}

void PointInfo::drawS(Widget w, GC gc)
{
    XSetForeground(XtDisplay(w), gc, BlackPixelOfScreen(XtScreen(w)));
    XSetBackground(XtDisplay(w), gc, WhitePixelOfScreen(XtScreen(w)));
    XSetLineAttributes(XtDisplay(w), gc, 0, LineDoubleDash, CapRound, JoinRound);
    XDrawPoint(XtDisplay(w), XtWindow(w), gc, mPos.x, mPos.y);
}

void PointInfo::undrawS(Widget w, GC gc)
{
    XSetFunction(XtDisplay(w), gc, GXinvert);
    drawS(w, gc);
    XSetFunction(XtDisplay(w), gc, GXcopy);
}

LineInfo::LineInfo(Point begMousePos, Point endMousePos, Pixel colourF, Pixel colourB,
                   unsigned int lineWidth, bool isDashed, bool isFilled) :
    Shape{colourF, colourB, lineWidth, isDashed, isFilled},
    mBegin{begMousePos}, mEnd{endMousePos}
{ }

void LineInfo::draw(Widget w, GC gc)
{
    XSetForeground(XtDisplay(w), gc, mColourF);
    XSetBackground(XtDisplay(w), gc, mColourB);
    XSetLineAttributes(XtDisplay(w), gc, mLineWidth, mDashed ? LineDoubleDash : LineSolid, CapRound, JoinRound);
    XDrawLine(XtDisplay(w), XtWindow(w), gc, mBegin.x, mBegin.y, mEnd.x, mEnd.y);
}

void LineInfo::drawS(Widget w, GC gc)
{
    XSetFunction(XtDisplay(w), gc, GXinvert);
    XSetBackground(XtDisplay(w), gc, BlackPixelOfScreen(XtScreen(w)));
    XSetForeground(XtDisplay(w), gc, WhitePixelOfScreen(XtScreen(w)));
    XSetLineAttributes(XtDisplay(w), gc, 0, LineOnOffDash, CapRound, JoinRound);
    XDrawLine(XtDisplay(w), XtWindow(w), gc, mBegin.x, mBegin.y, mEnd.x, mEnd.y);
    XSetFunction(XtDisplay(w), gc, GXcopy);
}

void LineInfo::undrawS(Widget w, GC gc)
{
    drawS(w, gc);
}

RectangleInfo::RectangleInfo(Point begMousePos, Point endMousePos, Pixel colourF, Pixel colourB,
                             unsigned int lineWidth, bool isDashed, bool isFilled) :
    Shape{colourF, colourB, lineWidth, isDashed, isFilled}
{
    if (begMousePos.x > endMousePos.x)
    {
        mBegin.x = endMousePos.x;
        mWidth = static_cast<unsigned int>(begMousePos.x - endMousePos.x);
    }
    else
    {
        mBegin.x = begMousePos.x;
        mWidth = static_cast<unsigned int>(endMousePos.x - begMousePos.x);
    }
    if (begMousePos.y > endMousePos.y)
    {
        mBegin.y = endMousePos.y;
        mHeight = static_cast<unsigned int>(begMousePos.y - endMousePos.y);
    }
    else
    {
        mBegin.y = begMousePos.y;
        mHeight = static_cast<unsigned int>(endMousePos.y - begMousePos.y);
    }
}

void RectangleInfo::draw(Widget w, GC gc)
{
    XSetForeground(XtDisplay(w), gc, mColourF);
    XSetBackground(XtDisplay(w), gc, mColourB);
    XSetLineAttributes(XtDisplay(w), gc, mLineWidth, mDashed ? LineDoubleDash : LineSolid, CapRound, JoinRound);
    if (mFilled)
    {
        XFillRectangle(XtDisplay(w), XtWindow(w), gc, mBegin.x, mBegin.y, mWidth, mHeight);
    }
    else
    {
        XDrawRectangle(XtDisplay(w), XtWindow(w), gc, mBegin.x, mBegin.y, mWidth, mHeight);
    }
}

void RectangleInfo::drawS(Widget w, GC gc)
{
    XSetFunction(XtDisplay(w), gc, GXinvert);
    XSetBackground(XtDisplay(w), gc, BlackPixelOfScreen(XtScreen(w)));
    XSetForeground(XtDisplay(w), gc, WhitePixelOfScreen(XtScreen(w)));
    XSetLineAttributes(XtDisplay(w), gc, 0, LineOnOffDash, CapRound, JoinRound);
    XDrawRectangle(XtDisplay(w), XtWindow(w), gc, mBegin.x, mBegin.y, mWidth, mHeight);
    XSetFunction(XtDisplay(w), gc, GXcopy);
}

void RectangleInfo::undrawS(Widget w, GC gc)
{
    drawS(w, gc);
}

EllipseInfo::EllipseInfo(Point begMousePos, Point endMousePos, Pixel colourF, Pixel colourB,
                         unsigned int lineWidth, bool isDashed, bool isFilled) :
    Shape{colourF, colourB, lineWidth, isDashed, isFilled}
{
    // Fixes visual artifacts for arcs with line width 0.
    mLineWidth = lineWidth == 0 ? 1 : lineWidth;

    mWidth = static_cast<unsigned int>(std::abs(begMousePos.x - endMousePos.x));
    mHeight = static_cast<unsigned int>(std::abs(begMousePos.y - endMousePos.y));

    mBegin.x = begMousePos.x - mWidth;
    mBegin.y = begMousePos.y - mHeight;

    mWidth = static_cast<unsigned int>(mWidth * 2.0);
    mHeight = static_cast<unsigned int>(mHeight * 2.0);
}

void EllipseInfo::draw(Widget w, GC gc)
{
    // TODO - move shared code.
    XSetFunction(XtDisplay(w), gc, GXcopy);
    XSetForeground(XtDisplay(w), gc, mColourF);
    XSetBackground(XtDisplay(w), gc, mColourB);
    XSetLineAttributes(XtDisplay(w), gc, mLineWidth, mDashed ? LineDoubleDash : LineSolid, CapRound, JoinRound);
    if (mFilled)
    {
        XFillArc(XtDisplay(w), XtWindow(w), gc, mBegin.x, mBegin.y, mWidth, mHeight, DEG_BEGIN, DEG_END);
    }
    else
    {
        XDrawArc(XtDisplay(w), XtWindow(w), gc, mBegin.x, mBegin.y, mWidth, mHeight, DEG_BEGIN, DEG_END);
    }
}

void EllipseInfo::drawS(Widget w, GC gc)
{
    XSetFunction(XtDisplay(w), gc, GXinvert);
    XSetBackground(XtDisplay(w), gc, BlackPixelOfScreen(XtScreen(w)));
    XSetForeground(XtDisplay(w), gc, WhitePixelOfScreen(XtScreen(w)));
    XSetLineAttributes(XtDisplay(w), gc, 1, LineSolid, CapRound, JoinRound);
    XDrawArc(XtDisplay(w), XtWindow(w), gc, mBegin.x, mBegin.y, mWidth, mHeight, DEG_BEGIN, DEG_END);
}

void EllipseInfo::undrawS(Widget w, GC gc)
{
    drawS(w, gc);
}

/**
 * Main application class.
 */
class Application
{
public:
    /**
     * Initialize the application.
     * @param argc
     * @param argv
     * @return Returns singleton instance of the application.
     */
    static Application& init(int argc, char* argv[]);

    /**
     * Run the application.
     */
    void run();
private:
    /// Private initialization constructor.
    Application(int argc, char* argv[]);

    /// Create the window and all UI elements.
    void createWindow(int argc, char* argv[]);
    Widget createMenu(Widget mainWin);
    Widget createDrawingArea(Widget mainWin);
    Widget createControlArea(Widget mainWin);
    void createQuitDialog(Widget topLevel);
    /// Create and configure the graphics context.
    void createGC();

    /**
     * Input event handler
     * @param w Registered widget.
     * @param clientData Additional data.
     * @param event Event specification.
     * @param cont Should the next handler be called?
     */
    static void ehInput(Widget w,
                        XtPointer clientData,
                        XEvent *event,
                        Boolean *cont);

    /**
     * Callback for expose.
     * @param w Registered widget.
     * @param clientData Additional data.
     * @param callData Callback specific data.
     */
    static void cbExpose(Widget w,
                         XtPointer clientData,
                         XtPointer callData);

    /**
     * Callback for drawing.
     * @param w Registered widget.
     * @param clientData Additional data.
     * @param callData Callback specific data.
     */
    static void cbDraw(Widget w,
                       XtPointer clientData,
                       XtPointer callData);

    /**
     * Callback from menu.
     * @param w Registered widget.
     * @param clientData Additional data.
     * @param callData Callback specific data.
     */
    static void cbFile(Widget w,
                       XtPointer clientData,
                       XtPointer callData);

    /**
     * Callback from selecting a shape.
     * @param w Registered widget.
     * @param clientData Additional data.
     * @param callData Callback specific data.
     */
    static void cbShapeSelect(Widget w,
                              XtPointer clientData,
                              XtPointer callData);

    /**
     * Callback from selecting foreground colour.
     * @param w Registered widget.
     * @param clientData Additional data.
     * @param callData Callback specific data.
     */
    static void cbFColourSelect(Widget w,
                                XtPointer clientData,
                                XtPointer callData);

    /**
     * Callback from selecting background colour.
     * @param w Registered widget.
     * @param clientData Additional data.
     * @param callData Callback specific data.
     */
    static void cbBColourSelect(Widget w,
                                XtPointer clientData,
                                XtPointer callData);

    /**
     * Callback from selecting dashed line.
     * @param w Registered widget.
     * @param clientData Additional data.
     * @param callData Callback specific data.
     */
    static void cbDashed(Widget w,
                         XtPointer clientData,
                         XtPointer callData);

    /**
     * Callback from selecting filled shape.
     * @param w Registered widget.
     * @param clientData Additional data.
     * @param callData Callback specific data.
     */
    static void cbFilled(Widget w,
                         XtPointer clientData,
                         XtPointer callData);

    /**
     * Callback from selecting line width.
     * @param w Registered widget.
     * @param clientData Additional data.
     * @param callData Callback specific data.
     */
    static void cbLineWidth(Widget w,
                            XtPointer clientData,
                            XtPointer callData);

    /**
     * Callback from closing the window.
     * @param w Registered widget.
     * @param clientData Additional data.
     * @param callData Callback specific data.
     */
    static void cbQuit(Widget w,
                       XtPointer clientData,
                       XtPointer callData);

    /**
     * Callback from quit dialog.
     * @param w Registered widget.
     * @param clientData Additional data.
     * @param callData Callback specific data.
     */
    static void cbQuitD(Widget w,
                        XtPointer clientData,
                        XtPointer callData);

    /**
     * Get colour based on index.
     * @param w Widget for which si the colour being selected.
     * @param index Index of the colour.
     * @return Value of the colour.
     */
    static Pixel getColour(Widget w, unsigned int index);

    static std::unique_ptr<Shape> createShape();

    static constexpr int WINDOW_MIN_WIDTH{600};
    static constexpr int WINDOW_MIN_HEIGHT{400};
    static constexpr int START_DA_WIDTH{800};
    static constexpr int START_DA_HEIGHT{800};
    static constexpr int M_CLEAR_INDEX{0};
    static constexpr int M_QUIT_INDEX{1};
    static const char* SHAPE_NAMES[];
    enum SHAPES
    {
        S_POINT_INDEX = 0,
        S_LINE_INDEX,
        S_RECTANGLE_INDEX,
        S_ELLIPSE_INDEX,
        NUM_SHAPE_NAMES
    };
    static const char* COLOUR_NAMES[];
    enum COLOURS
    {
        C_BLACK = 0,
        C_WHITE,
        C_RED,
        C_GREEN,
        C_BLUE,
        C_YELLOW,
        C_PINK,
        C_GREY,
        NUM_COLOUR_NAMES
    };
    static constexpr unsigned int DEFAULT_FG_COLOUR{C_BLACK};
    static constexpr unsigned int DEFAULT_BG_COLOUR{C_WHITE};
    static constexpr unsigned int QUIT_DIALOG_OK{0};
    static constexpr unsigned int QUIT_DIALOG_CANCEL{1};

    /**
     * Container for "global" variables.
     */
    static struct GlobalContext
    {
        /// Main application context.
        XtAppContext appContext;
        /**
         * Represents the current state of the button.
         *  0 - Not pressed
         *  1 - Pressed
         *  2 - Press has been processed
         */
        int buttonPressed{0};
        /// Position of the mouse on button press.
        Point pressPos;
        /// Position of the mouse on button release;
        Point currentPos;
        /// Should the line be dashed?
        bool dashed{false};
        /// Should the shape be filled?
        bool filled{false};
        /// Line width value.
        unsigned int lineWidth{0};
        /// Foreground colour.
        Pixel foreground{0};
        /// Background colour.
        Pixel background{0};
        /// Currently selected shape type.
        int currentShape{S_POINT_INDEX};
        /// Drawing area widget.
        Widget drawArea;
        /// Main graphics context.
        GC gc;
        /// Graphics context configuration.
        XGCValues gcv;
        /// Dialog asking the user about quitting.
        Widget quitDialog;
        /// List of currently displayed permanent shapes.
        std::vector<std::unique_ptr<Shape>> shapes;
        /// Shape being currently drawn.
        std::unique_ptr<Shape> currentDrawShape;
    } sG;

    ///  Number of command line arguments.
    int mArgc{0};
    /// Vector of command line arguments.
    char** mArgv{nullptr};
protected:
};

// Implementation:

Application::GlobalContext Application::sG;
const char* Application::SHAPE_NAMES[] = {
    "Point",
    "Line",
    "Rectangle",
    "Ellipse"
};
const char* Application::COLOUR_NAMES[] = {
    "Black",
    "White",
    "Red",
    "Green",
    "Blue",
    "Yellow",
    "Pink",
    "Grey"
};

Application &Application::init(int argc, char** argv)
{
    static Application app(argc, argv);
    return app;
}

Application::Application(int argc, char** argv) :
    mArgc{argc}, mArgv{argv}
{
    createWindow(argc, argv);
    createGC();
}

void Application::createWindow(int argc, char* argv[])
{
    /*
     * Register the default language procedure
     */
    XtSetLanguageProc(NULL, (XtLanguageProc) NULL, NULL);

    char* stringSettings[] = {
        "*.quitDialog.dialogTitle: Quit application",
        "*.quitDialog.messageString: Are you sure you want to quit?",
        "*.quitDialog.okLabelString: Yes",
        "*.quitDialog.cancelLabelString: No",
        "*.quitDialog.messageAlignment: XmALIGNMENT_CENTER",
        NULL
    };

    Widget topLevel = XtVaAppInitialize(
        &sG.appContext,            /* Application context */
        "Draw",                /* Application class */
        NULL, 0,                /* command line option list */
        &mArgc, mArgv,            /* command line args */
        stringSettings,                /* for missing app-defaults file */
        XmNminWidth, WINDOW_MIN_WIDTH,
        XmNminHeight, WINDOW_MIN_HEIGHT,
        XmNdeleteResponse, XmDO_NOTHING,
        NULL);                /* terminate varargs list */

    Widget mainWin = XtVaCreateManagedWidget(
        "mainWin",            /* widget name */
        xmMainWindowWidgetClass,        /* widget class */
        topLevel,                /* parent widget*/
        XmNcommandWindowLocation, XmCOMMAND_BELOW_WORKSPACE,
        NULL);                /* terminate varargs list */

    Widget mainForm = XtVaCreateManagedWidget(
        "mainForm",
        xmFormWidgetClass,
        mainWin,
        NULL
    );

    Widget menu = createMenu(mainWin);
    Widget rowColumn = createControlArea(mainForm);
    Widget frame = createDrawingArea(mainForm);

    // Set default colours
    sG.foreground = getColour(sG.drawArea, DEFAULT_FG_COLOUR);
    sG.background = getColour(sG.drawArea, DEFAULT_BG_COLOUR);

    createQuitDialog(topLevel);

    // Catch window close event.
    Atom wmDelete = XInternAtom(XtDisplay(topLevel), "WM_DELETE_WINDOW", false);
    XmAddWMProtocolCallback(topLevel, wmDelete, Application::cbQuit, NULL);
    XmActivateWMProtocol(topLevel, wmDelete);

    //XmMainWindowSetAreas(mainWin, menu, rowColumn, NULL, NULL, frame);
    XmMainWindowSetAreas(mainWin, menu, NULL, NULL, NULL, mainForm);

    XtRealizeWidget(topLevel);
}

Widget Application::createMenu(Widget mainWin)
{
    XmString fileStr = XmStringCreateLocalized(C_STR("File"));

    Widget menuBar = XmVaCreateSimpleMenuBar(
        mainWin, C_STR("menuBar"),
        XmVaCASCADEBUTTON, fileStr, 'F',
        NULL);
    XtManageChild(menuBar);
    XmStringFree(fileStr);

    XmString clearStr = XmStringCreateLocalized(C_STR("Clear"));
    XmString quitStr = XmStringCreateLocalized(C_STR("Quit"));

    XmVaCreateSimplePulldownMenu(
        menuBar, C_STR("fileMenu"), 0, Application::cbFile,
        XmVaPUSHBUTTON, clearStr, 'C', NULL, NULL,
        XmVaSEPARATOR,
        XmVaPUSHBUTTON, quitStr, 'Q', NULL, NULL,
        NULL
    );
    XmStringFree(clearStr);
    XmStringFree(quitStr);

    return menuBar;
}

Widget Application::createDrawingArea(Widget mainWin)
{
    Widget frame = XtVaCreateManagedWidget(
        "frame",                /* widget name */
        xmFrameWidgetClass,        /* widget class */
        mainWin,                /* parent widget */
        XmNtopAttachment, XmATTACH_FORM,
        XmNrightAttachment, XmATTACH_FORM,
        XmNleftAttachment, XmATTACH_FORM,
        XmNbottomAttachment, XmATTACH_FORM,
        NULL);                /* terminate varargs list */

    sG.drawArea = XtVaCreateManagedWidget(
        "drawingArea",            /* widget name */
        xmDrawingAreaWidgetClass,        /* widget class */
        frame,                /* parent widget*/
        XmNwidth, START_DA_WIDTH,            /* set startup width */
        XmNheight, START_DA_HEIGHT,            /* set startup height */
        XmNbackground, WhitePixelOfScreen(XtScreen(mainWin)),
        XmNresizePolicy, XmRESIZE_ANY,
        NULL);                /* terminate varargs list */

    /*
    XSelectInput(XtDisplay(drawArea), XtWindow(drawArea),
      KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask
      | Button1MotionMask );
    */

    XtAddCallback(sG.drawArea, XmNinputCallback, Application::cbDraw, sG.drawArea);
    XtAddEventHandler(sG.drawArea, ButtonMotionMask, False, Application::ehInput, NULL);
    XtAddCallback(sG.drawArea, XmNexposeCallback, Application::cbExpose, sG.drawArea);

    return frame;
}

Widget Application::createControlArea(Widget mainWin)
{
    Widget rowColumn = XtVaCreateManagedWidget(
        "rowColumn",            /* widget name */
        xmRowColumnWidgetClass,        /* widget class */
        mainWin,                /* parent widget */
        XmNentryAlignment, XmALIGNMENT_CENTER,    /* alignment */
        XmNorientation, XmVERTICAL,    /* orientation */
        XmNpacking, XmPACK_TIGHT,    /* packing mode */
        XmNtopAttachment, XmATTACH_FORM,
        XmNrightAttachment, XmATTACH_FORM,
        XmNleftAttachment, XmATTACH_NONE,
        XmNbottomAttachment, XmATTACH_NONE,
        XmNadjustLast, false,
        NULL);                /* terminate varargs list */

    // Container for shape settings.
    Widget shapeFrame = XtVaCreateManagedWidget(
        "shapeFrame",
        xmFrameWidgetClass,
        rowColumn,
        XmNshadowType, XmSHADOW_IN,
        NULL
    );

    // Create title label for the frame.
    XmString tShapeLStr = XmStringCreateLocalized(C_STR("Shape"));
    Widget tShapeL = XtVaCreateManagedWidget(
        "tLineL",
        xmLabelWidgetClass,
        shapeFrame,
        XmNlabelString, tShapeLStr,
        XmNframeChildType, XmFRAME_TITLE_CHILD,
        XmNchildVerticalAlignment, XmALIGNMENT_CENTER,
        NULL
    );
    XmStringFree(tShapeLStr);

    // Create a Bulletin Board for the settings.
    Widget shapeFrameBB = XtVaCreateManagedWidget(
        "shapeFrameBB",
        //xmBulletinBoardWidgetClass,
        xmRowColumnWidgetClass,
        shapeFrame,
        XmNorientation, XmVERTICAL,
        XmNentryAlignment, XmALIGNMENT_CENTER,
        NULL
    );

    // Create a ComboBox for selecting shape types.
    XmStringTable shapeList = reinterpret_cast<XmStringTable>(
        XtMalloc(NUM_SHAPE_NAMES * sizeof(XmString*)));
    for (int iii = 0; iii < NUM_SHAPE_NAMES; ++iii)
    {
        shapeList[iii] = XmStringCreateLocalized(const_cast<char*>(SHAPE_NAMES[iii]));
    }
    Arg args[4];
    XtSetArg(args[0], XmNitems, shapeList);
    XtSetArg(args[1], XmNitemCount, NUM_SHAPE_NAMES);
    Widget shapeCB = XmCreateDropDownComboBox(shapeFrameBB, C_STR("shapeCB"), args, 2);
    for (int iii = 0; iii < NUM_SHAPE_NAMES; ++iii)
    {
        XmStringFree(shapeList[iii]);
    }
    XtFree(reinterpret_cast<char*>(shapeList));
    XtAddCallback(shapeCB, XmNselectionCallback, Application::cbShapeSelect, NULL);
    XtManageChild(shapeCB);

    // Create CheckBox for selecting filled shape type.
    XmString filledStr = XmStringCreateLocalized(C_STR("Filled"));
    Widget filledCB = XmVaCreateSimpleCheckBox(
        shapeFrameBB, C_STR("filledCB"),
        Application::cbFilled,
        XmVaCHECKBUTTON, filledStr, NULL, NULL, NULL,
        NULL
    );
    XmStringFree(filledStr);
    XtManageChild(filledCB);

    // Container for line settings.
    Widget lineFrame = XtVaCreateManagedWidget(
        "lineFrame",
        xmFrameWidgetClass,
        rowColumn,
        XmNshadowType, XmSHADOW_IN,
        NULL
    );

    // Create title label for the frame.
    XmString tLineLStr = XmStringCreateLocalized(C_STR("Line"));
    Widget tLineL = XtVaCreateManagedWidget(
        "tLineL",
        xmLabelWidgetClass,
        lineFrame,
        XmNlabelString, tLineLStr,
        XmNframeChildType, XmFRAME_TITLE_CHILD,
        XmNchildVerticalAlignment, XmALIGNMENT_CENTER,
        NULL
    );
    XmStringFree(tLineLStr);

    // Create a Bulletin Board for the settings.
    Widget lineFrameBB = XtVaCreateManagedWidget(
        "lineFrameBB",
        //xmBulletinBoardWidgetClass,
        xmRowColumnWidgetClass,
        lineFrame,
        XmNorientation, XmVERTICAL,
        XmNentryAlignment, XmALIGNMENT_CENTER,
        NULL
    );

    /*
    // Labels specifying which colour is being selected.
    XmString wLineLStr = XmStringCreateLocalized(C_STR("Width"));
    Widget wLineL = XtVaCreateManagedWidget(
        "wLineL",
        xmLabelWidgetClass,
        lineFrameBB,
        XmNlabelString, wLineLStr,
        NULL
    );
    XmStringFree(wLineLStr);

    // Create SpinBox for changing the line width.
    Widget lineWidthSB = XmVaCreateSimpleSpinBox(
        lineFrameBB, C_STR("LineWidthSB"),
        XmNspinBoxChildType, XmNUMERIC,
        XmNminimumValue, 0,
        XmNmaximumValue, 10,
        XmNincrementValue, 1,
        XmNeditable, false,
        XmNcolumns, 10,
        XmNwrap, false,
        NULL
    );
    XtAddCallback(lineWidthSB, XmNvalueChangedCallback, Application::cbLineWidth, NULL);
    XtManageChild(lineWidthSB);
     */

    // Create scale for changing the line width.
    XmString lineWidthSCStr = XmStringCreateLocalized(C_STR("Width"));
    Widget lineWidthSC = XmVaCreateManagedScale(
        lineFrameBB, C_STR("lineWidthSC"),
        XmNminimum, 0,
        XmNmaximum, 10,
        XmNorientation, XmHORIZONTAL,
        XmNshowValue, true,
        XmNtitleString, lineWidthSCStr,
        NULL
    );
    XmStringFree(lineWidthSCStr);
    XtAddCallback(lineWidthSC, XmNdragCallback, Application::cbLineWidth, NULL);

    // Create CheckBox for selecting dashed line type.
    XmString dashedStr = XmStringCreateLocalized(C_STR("Dashed"));
    Widget dashedCB = XmVaCreateSimpleCheckBox(
        lineFrameBB, C_STR("dashedCB"),
        Application::cbDashed,
        XmVaCHECKBUTTON, dashedStr, NULL, NULL, NULL,
        NULL
    );
    XmStringFree(dashedStr);
    XtManageChild(dashedCB);

    // Container for colour selection.
    Widget colourFrame = XtVaCreateManagedWidget(
        "colourFrame",
        xmFrameWidgetClass,
        rowColumn,
        XmNshadowType, XmSHADOW_IN,
        NULL
    );

    // Create title label for the frame.
    XmString tColourLStr = XmStringCreateLocalized(C_STR("Colours"));
    Widget tColourL = XtVaCreateManagedWidget(
        "tColourL",
        xmLabelWidgetClass,
        colourFrame,
        XmNlabelString, tColourLStr,
        XmNframeChildType, XmFRAME_TITLE_CHILD,
        XmNchildVerticalAlignment, XmALIGNMENT_CENTER,
        NULL
    );
    XmStringFree(tColourLStr);

    // Create a Bulletin Board for the settings.
    Widget colourFrameBB = XtVaCreateManagedWidget(
        "colourFrameBB",
        //xmBulletinBoardWidgetClass,
        xmRowColumnWidgetClass,
        colourFrame,
        XmNorientation, XmVERTICAL,
        XmNentryAlignment, XmALIGNMENT_CENTER,
        NULL
    );

    // Labels specifying which colour is being selected.

    // Create ComboBoxes for selecting the colours.
    XmStringTable colourList = reinterpret_cast<XmStringTable>(
        XtMalloc(NUM_COLOUR_NAMES * sizeof(XmString*)));
    for (int iii = 0; iii < NUM_COLOUR_NAMES; ++iii)
    {
        colourList[iii] = XmStringCreateLocalized(const_cast<char*>(COLOUR_NAMES[iii]));
    }
    XtSetArg(args[0], XmNitems, colourList);
    XtSetArg(args[1], XmNitemCount, NUM_COLOUR_NAMES);
    //Widget colourCB = XmCreateDropDownComboBox(rowColumn, C_STR("colourCB"), args, 2);
    XtSetArg(args[2], XmNselectedPosition, DEFAULT_FG_COLOUR);

    // First, create the label.
    XmString fColourLStr = XmStringCreateLocalized(C_STR("Foreground"));
    Widget fColourL = XtVaCreateWidget(
        "fColourL",
        xmLabelWidgetClass,
        colourFrameBB,
        XmNlabelString, fColourLStr,
        NULL
    );
    XmStringFree(fColourLStr);
    // Then, create the combo box.
    Widget fColourCB = XmCreateDropDownComboBox(colourFrameBB, C_STR("fColourCB"), args, 3);

    // First, create the label.
    XmString bColourLStr = XmStringCreateLocalized(C_STR("Background"));
    Widget bColourL = XtVaCreateWidget(
        "bColourL",
        xmLabelWidgetClass,
        colourFrameBB,
        XmNlabelString, bColourLStr,
        NULL
    );
    XmStringFree(bColourLStr);
    // Then, create the combo box.
    XtSetArg(args[2], XmNselectedPosition, DEFAULT_BG_COLOUR);
    Widget bColourCB = XmCreateDropDownComboBox(colourFrameBB, C_STR("bColourCB"), args, 3);

    for (int iii = 0; iii < NUM_COLOUR_NAMES; ++iii)
    {
        XmStringFree(colourList[iii]);
    }
    XtFree(reinterpret_cast<char*>(colourList));
    XtManageChild(fColourL);
    XtAddCallback(fColourCB, XmNselectionCallback, Application::cbFColourSelect, NULL);
    XtManageChild(fColourCB);
    XtManageChild(bColourL);
    XtAddCallback(bColourCB, XmNselectionCallback, Application::cbBColourSelect, NULL);
    XtManageChild(bColourCB);

    return rowColumn;
}

void Application::createQuitDialog(Widget topLevel)
{
    sG.quitDialog = XmCreateQuestionDialog(
        topLevel, C_STR("quitDialog"),
        NULL, 0
    );
    XtVaSetValues(
        sG.quitDialog,
        XmNdialogStyle, XmDIALOG_FULL_APPLICATION_MODAL,
        NULL
    );
    XtUnmanageChild(XmMessageBoxGetChild(sG.quitDialog, XmDIALOG_HELP_BUTTON));
    XtUnmanageChild(sG.quitDialog);
    XtAddCallback(sG.quitDialog, XmNokCallback, Application::cbQuitD, reinterpret_cast<XtPointer>(QUIT_DIALOG_OK));
    XtAddCallback(sG.quitDialog, XmNcancelCallback, Application::cbQuitD, reinterpret_cast<XtPointer>(QUIT_DIALOG_CANCEL));
}

void Application::createGC()
{
    sG.gcv.foreground = BlackPixelOfScreen(XtScreen(sG.drawArea));
    sG.gcv.background = WhitePixelOfScreen(XtScreen(sG.drawArea));
    sG.gc = XCreateGC(XtDisplay(sG.drawArea),
                      RootWindowOfScreen(XtScreen(sG.drawArea)),
                      GCForeground,
                      &sG.gcv);
    XSetForeground(XtDisplay(sG.drawArea), sG.gc, sG.gcv.foreground);
    XSetBackground(XtDisplay(sG.drawArea), sG.gc, sG.gcv.background);
}

void Application::run()
{
    XtAppMainLoop(sG.appContext);
}

void Application::ehInput(Widget w, XtPointer clientData, XEvent *event, Boolean *cont)
{
    if (sG.buttonPressed)
    {
        /*
        Pixel fg;
        Pixel bg;
        XSetFunction(XtDisplay(w), sG.gc, GXxor);
        XSetPlaneMask(XtDisplay(w), sG.gc, ~0);
        XtVaGetValues(w, XmNforeground, &fg, XmNbackground, &bg, NULL);
        XSetForeground(XtDisplay(w), sG.gc, fg ^ bg);
        */

        Point &p1{sG.pressPos};
        Point &p2{sG.currentPos};

        p2.x = event->xmotion.x;
        p2.y = event->xmotion.y;

        if (sG.currentDrawShape)
        {
            sG.currentDrawShape->undrawS(sG.drawArea, sG.gc);
            sG.currentDrawShape.release();
        }

        sG.currentDrawShape = createShape();
        sG.currentDrawShape->drawS(sG.drawArea, sG.gc);

        /*
        if (sG.buttonPressed > 1)
        {
            XDrawLine(XtDisplay(w), XtWindow(w), sG.gc, p1.x, p1.y, p2.x, p2.y);
        }
        else
        {
            sG.buttonPressed = 2;
        }
        XDrawLine(XtDisplay(w), XtWindow(w), sG.gc, p1.x, p1.y, p2.x, p2.y);
         */
    }
}

void Application::cbExpose(Widget w, XtPointer clientData, XtPointer callData)
{
    //XDrawSegments(XtDisplay(w), XtWindow(w), sG.gc, sG.lines.data(), sG.lines.size());
    for (auto &shape : sG.shapes)
    {
        shape->draw(sG.drawArea, sG.gc);
    }
}

void Application::cbDraw(Widget w, XtPointer clientData, XtPointer callData)
{
    XGCValues v;
    XmDrawingAreaCallbackStruct *d = static_cast<XmDrawingAreaCallbackStruct*>(callData);
    Point &p1{sG.pressPos};
    Point &p2{sG.currentPos};

    switch (d->event->type)
    {
        case ButtonPress:
            if (d->event->xbutton.button == Button1)
            {
                sG.buttonPressed = 1;
                p1.x = d->event->xbutton.x;
                p1.y = d->event->xbutton.y;
                break;
            }

        case ButtonRelease:
            if (d->event->xbutton.button == Button1)
            {
                if (sG.currentDrawShape)
                {
                    sG.currentDrawShape->undrawS(w, sG.gc);
                    sG.currentDrawShape.release();
                }

                p2.x = d->event->xbutton.x;
                p2.y = d->event->xbutton.y;
                auto shape = createShape();
                shape->draw(sG.drawArea, sG.gc);
                sG.shapes.emplace_back(std::move(shape));
                std::cout << sG.shapes.size() << std::endl;

                sG.buttonPressed = 0;
                break;
            }
    }
}

void Application::cbFile(Widget w, XtPointer clientData, XtPointer callData)
{
    intptr_t choice = (intptr_t) clientData;

    switch (choice)
    {
        case M_CLEAR_INDEX:
        {
            Widget wcd = sG.drawArea;

            sG.shapes.clear();
            XClearWindow(XtDisplay(wcd), XtWindow(wcd));

            break;
        }
        case M_QUIT_INDEX:
        {
            cbQuit(w, clientData, callData);
            break;
        }
        default:
        {
            std::cerr << "Unknown menu item called back!" << std::endl;
            break;
        }
    }
}

void Application::cbShapeSelect(Widget w, XtPointer clientData, XtPointer callData)
{
    XmComboBoxCallbackStruct *cb = reinterpret_cast<XmComboBoxCallbackStruct*>(callData);

    sG.currentShape = cb->item_position;
}

void Application::cbFColourSelect(Widget w, XtPointer clientData, XtPointer callData)
{
    XmComboBoxCallbackStruct *data = reinterpret_cast<XmComboBoxCallbackStruct*>(callData);
    sG.foreground = getColour(sG.drawArea, data->item_position);
}

void Application::cbBColourSelect(Widget w, XtPointer clientData, XtPointer callData)
{
    XmComboBoxCallbackStruct *data = reinterpret_cast<XmComboBoxCallbackStruct*>(callData);
    sG.background = getColour(sG.drawArea, data->item_position);
}

void Application::cbDashed(Widget w, XtPointer clientData, XtPointer callData)
{
    XmToggleButtonCallbackStruct *state = reinterpret_cast<XmToggleButtonCallbackStruct*>(callData);
    sG.dashed = state->set;
}

void Application::cbFilled(Widget w, XtPointer clientData, XtPointer callData)
{
    XmToggleButtonCallbackStruct *state = reinterpret_cast<XmToggleButtonCallbackStruct*>(callData);
    sG.filled = state->set;
}

void Application::cbLineWidth(Widget w, XtPointer clientData, XtPointer callData)
{
    XmScaleCallbackStruct *state = reinterpret_cast<XmScaleCallbackStruct*>(callData);
    sG.lineWidth = state->value;
}

void Application::cbQuit(Widget w, XtPointer clientData, XtPointer callData)
{
    std::cout << "Displaying dialog" << std::endl;
    XtManageChild(sG.quitDialog);
}

void Application::cbQuitD(Widget w, XtPointer clientData, XtPointer callData)
{
    intptr_t answer = reinterpret_cast<intptr_t>(clientData);
    switch (answer)
    {
        case QUIT_DIALOG_OK:
        {
            std::cout << "Destroying the application..." << std::endl;
            exit(0);
            break;
        }
        case QUIT_DIALOG_CANCEL:
        {
            break;
        }
        default:
        {
            std::cerr << "Unknown answer returned from quit dialog! " << answer << std::endl;
            break;
        }
    }
}

Pixel Application::getColour(Widget w, unsigned int index)
{
    if (index > NUM_COLOUR_NAMES)
    {
        std::cerr << "Colour index out of bounds!" << std::endl;
        return 0;
    }

    XColor xcolor;
    XColor unused;
    if (XAllocNamedColor(XtDisplay(w), DefaultColormapOfScreen(XtScreen(w)),
                         COLOUR_NAMES[index], &xcolor, &unused) == 0)
    {
        std::cerr << "Unknown colour " << COLOUR_NAMES[index] << std::endl;
    }

    return xcolor.pixel;
}

std::unique_ptr<Shape> Application::createShape()
{
    Shape* result{nullptr};
    switch (sG.currentShape)
    {
        case S_POINT_INDEX:
        {
            if (sG.lineWidth > 0)
            {
                Point simulated(
                    sG.pressPos.x + sG.lineWidth, 
                    sG.pressPos.y + sG.lineWidth
                );
                result = new EllipseInfo(sG.pressPos,
                                         simulated, 
                                         sG.foreground, sG.background, 0, false, true);
            }
            else
            {
                result = new PointInfo(sG.pressPos, sG.foreground, sG.background, sG.lineWidth, sG.dashed, sG.filled);
            }
            break;
        }
        case S_LINE_INDEX:
        {
            // TODO - change 4 to selectable value.
            result = new LineInfo(sG.pressPos, sG.currentPos, sG.foreground, sG.background, sG.lineWidth, sG.dashed, sG.filled);
            break;
        }
        case S_RECTANGLE_INDEX:
        {
            result = new RectangleInfo(sG.pressPos, sG.currentPos, sG.foreground, sG.background, sG.lineWidth, sG.dashed, sG.filled);
            break;
        }
        case S_ELLIPSE_INDEX:
        {

            result = new EllipseInfo(sG.pressPos, sG.currentPos, sG.foreground, sG.background, sG.lineWidth, sG.dashed, sG.filled);
            break;
        }
        default:
        {
            std::cerr << "Selected unknown shape!" << std::endl;
            break;
        }
    }

    return std::unique_ptr<Shape>(result);
}

int main(int argc, char* argv[])
{
    try {
        Application &app{Application::init(argc, argv)};
        app.run();
    } catch (std::runtime_error &e) {
        std::cout << "Exception has been thrown: " << e.what() << std::endl;
        return -1;
    }

    return 0;
}

